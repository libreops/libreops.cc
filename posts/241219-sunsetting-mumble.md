title: Sunsetting Mumble
slug: sunsetting-mumble
date: 2024-12-19 15:11:00
tags: service, mumble
files: mumble.png
---

[Mumble](https://www.mumble.info/) is a low-latency, high quality voice chat software.

It has been part of our services since the early years and it has worked really well for communities
that were looking for a way to hold audio conferences.

But at some point we also started offering a [video conference](https://meet.libreops.cc/) service,
based on [Jitsi](https://jitsi.org/), that has gradually made Mumble redundant and a service not actively being used anymore.

With that in mind, we decided to **terminate the service at January 31st**.
That would also help us move our focus on improving existing services that many communities depend on.

![mumble](mumble.png)

----

*If you value our work, please consider [supporting us](https://opencollective.com/libreops/). We only rely on our contributors.*
